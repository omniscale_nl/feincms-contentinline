# -*- coding: utf-8 -*-
import os
from setuptools import setup
from feincms_contentinline import __version__

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='feincms-contentinline',
    version=__version__,
    packages=['feincms_contentinline'],
    include_package_data=True,
    license='BSD License',  # example license
    description='A small FeinCMS addon to allow for inlines within contenttypes',
    long_description=README,
    url='https://bitbucket.org/omniscale_nl/feincms-contentinline',
    author='Timo Grevers',
    author_email='timo@omniscale.nl',
    install_requires=[
        'feincms>=1.11,<1.12'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
