(function ($) {

    var id_needle = '__feincmscontentinline__';

    function updateElementIndex(el, group, prefix, index) {

        // prefix = typeof prefix !== 'undefined' ? prefix : undefined;
        // index = typeof index !== 'undefined' ? index : undefined;

        var fix_inline = (undefined !== group && undefined !== index);

        if(fix_inline) {

            var id_regex = new RegExp('(' + group + '-(\\d+|__prefix__))');
            var replacement = group + '-' + index;
        }

        var
            $el = $(el),
            props = ['for'],
            attrs = ['id', 'name', 'data-id', 'data-field-id'];

        for(var i in props) {

            var p = props[i];

            if($el.prop(p)) {

                if(fix_inline) $el.prop(p, $el.prop(p).replace(id_regex, replacement));

                $el.prop(p, $el.prop(p).replace(id_needle, group));
            }
        }

        for(var i in attrs) {

            var a = attrs[i];

            if($el.attr(a)) {

                if(fix_inline) $el.attr(a, $el.prop(a).replace(id_regex, replacement));

                $el.attr(a, $el.prop(a).replace(id_needle, group));
            }
        }
    }

    function updateRow(el, prefix, group, ndx) {

        var $row = $(el);

        $row.removeClass('empty-form row1 row2');

        $row.parent().find('tr')
            .filter(':even').addClass('row1').end()
            .filter(':odd').addClass('row2');

        $row.attr('id', prefix + '-' + ndx);
        $row.attr('id', $row.attr('id').replace(id_needle, group));

        $row.find('*').each(function() {
            updateElementIndex(this, prefix, group, ndx);
        });
    }

    function add(e) {

        e.preventDefault();

        var $trigger = $(this);
        var $parent = $trigger.parent();
        var prefix = $parent.attr('id').replace('-group', '');
        var $totalForms = $('#id_' + prefix + '-TOTAL_FORMS');
        var $template = $('#' + prefix + '-empty');
        var $row = $template.clone(true);
        var nextIndex = parseInt($totalForms.val(), 10);

        $row.insertBefore($template);

        updateRow($row[0], prefix, 'OS' + $parent.closest('fieldset.order-item').index(), nextIndex);

        $totalForms.val(nextIndex + 1);
    }

    function submit(form, callback) {

        var callback = callback || function () {};
        var $form = $(form);
        var $parent = $form.parent();
        var group = 'OS' + $parent.parent().closest('fieldset.order-item').index();

        var data = $form.serializeArray()
            cleanData = {};

        for(var i = 0, c = data.length; i<c; i++) {

            var id_regex = new RegExp('(OS\\d+)');
            cleanData[data[i].name.replace(id_regex, id_needle)] = data[i].value;
        }

        $.ajax({
            type: $form.attr('method'),
            url:  $form.attr('action'),
            data: cleanData,
            success: function (data) {

                if('object' === typeof(data)) {

                    var $inlineLink = $parent.find('.form-row.inline_link input');

                    $inlineLink.val(data.pk);

                    getForm(data.url, $parent[0], function() {

                        $form.remove();
                        callback();
                    });

                } else {

                    var $returnedForm = $(data).find('form');

                    if($returnedForm.length > 0) {

                        $parent.find('.errorlist').remove();
                        $parent.find('.form-row').removeClass('errors');

                        $preparedForm = prepareForm($returnedForm[0], group);

                        $preparedForm.find('.errorlist').each(function() {

                            var $ul = $(this);
                            var id = $ul.parent().find('input').attr('id');
                            var $row = $parent.find('#' + id).closest('.form-row');

                            $row.addClass('errors').prepend($ul);
                        });
                    }
                }
            }
        });
    }

    function prepareForm(form, group) {

        var $form = $(form);

        $form.find('script').remove();
        $form.find('.submit-row').remove();

        var selectors = [
            '*[id*="__feincmscontentinline__"]',
            '*[name*="__feincmscontentinline__"]',
            '*[for*="__feincmscontentinline__"]',
            '*[data-field-id="__feincmscontentinline__"]',
            '*[data-id="__feincmscontentinline__"]',
        ];

        for(var i=0, c=selectors.length; i<c; i++) {

            $form.find(selectors[i]).each(function() {
                updateElementIndex(this, group);
            });
        }

        var $inlines = $form.find('.inline-group');

        if($inlines.length > 0) {

            $inlines.each(function(k, v) {

                var label = $(v).find('.empty-form h3 b').html().replace(':', '').toLowerCase();

                $inlines.css('boxShadow', 'none');

                $('<a></a>')
                    .addClass('omni-add-row')
                    .html('Voeg nog een ' + label + ' toe')
                    .appendTo(v);
            });
        }

        filer_init($form[0]);

        return $form;
    }

    function getForm(url, fieldset, callback) {

        var callback = callback || function () {};
        var $parent = $(fieldset);
        var group = 'OS' + $parent.parent().closest('fieldset.order-item').index();

        $.get(url, function (data) {

            var $form = $(data).find('form');

            if($form.length > 0) {

                var $preparedForm = prepareForm($form[0], group);

                $preparedForm
                    .appendTo($parent)
                    .attr('action', url)
                    .css('margin', '5px');

                callback();
            }

            $(window).trigger('content_type:loaded');
        });
    }

    $(window).on('load', function (e) {

      $('<div></div>', { 'id': 'feincmscontentinline_loading' })
        .append(
            $('<div></div>', { 'id': 'feincmscontentinline_loading_spinner' })
              .append($('<div></div>').addClass('bounce1'))
              .append($('<div></div>').addClass('bounce2'))
              .append($('<div></div>').addClass('bounce3'))
        ).hide().appendTo(document.body);

        $('#page_form .submit-row').on('click', 'input[type=submit]', function (e) {
            $(this).attr('checked', 'checked');
        });

        $('#page_form').on('submit', function (e) {

            var $forms = $('.item-content form');

            if($forms.length > 0) {

                $('#feincmscontentinline_loading').fadeIn();

                e.preventDefault();

                $forms.each(function (k, v) {

                    var $form = $(this);

                    $form.attr('processing', 'processing');

                    submit($form[0], function () {

                        if($('form[processing=processing]').length == 0 && $('.hooked .form .errorlist').length == 0) {

                            var $form = $('#page_form').unbind('submit');
                            $form.find('.submit-row input[type=submit][checked=checked]').trigger('click');
                        }
                    });
                });
            }
        });

        $('.order-machine.ui-sortable').on('click', 'a.omni-add-row', add);

        $('.order-machine.ui-sortable').bind('DOMNodeInserted', function(e) {

            var $el = $(e.target);

            if($el.find('.feincmscontentinline_hook').length > 0) {

                if(false == $el.hasClass('hooked')) {

                    var $parent = $($el.find('.feincmscontentinline_hook')[0]).parent();
                    var $hook = $($el.find('.feincmscontentinline_hook input')[0]);
                    var url = '/admin/feincms_contentinline/' + $hook.val() + '/add/?_popup=1&_json=1';

                    getForm(url, $parent[0]);
                }
            }
        });

        var $contentinlines = $('.order-machine.ui-sortable .module.order-item').has('.feincmscontentinline_hook');
        var contentinlines_count = $contentinlines.length;

        if(contentinlines_count > 0) {

            // track the index manually, since we are not using a normal loop
            var i = 0;

            $contentinlines.each(function (index, el) {

                if(0 == i) {
                  $('#feincmscontentinline_loading').fadeIn();
                }

                var $el = $(el);

                $el.addClass('hooked');

                var $parent = $($el.find('.feincmscontentinline_hook')[0]).parent();
                var $hook = $($el.find('.feincmscontentinline_hook input')[0]);
                var $inlineLink = $parent.find('.form-row.inline_link input');
                var url = '/admin/feincms_contentinline/' + $hook.val() + '/' + $inlineLink.val() + '/?_popup=1&_json=1';
                var callback = function () {}

                if(i + 1 == contentinlines_count) {
                    callback = function () {
                        $('#feincmscontentinline_loading').fadeOut();

                        $(window).trigger('done_loading');
                    };
                }

                getForm(url, $parent[0], callback);

                i++;
            });
        }
    });

})(django.jQuery);
