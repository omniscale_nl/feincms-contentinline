# -*- coding: utf-8 -*-

import json

from django import forms
from django.contrib import admin
from django.contrib.messages import get_messages
from django.core.urlresolvers import reverse
from django.forms.models import BaseInlineFormSet
from django.http import HttpResponse
from feincms_contentinline.models import FeinCMSContentInlineLink
from feincms_contentinline import settings


class FeinCMSContentInlineTypeForm(forms.ModelForm):
    """
    We need to define a new form to overrule the default id-prefix for the DOM
    identifiers.
    """

    def __init__(self, *args, **kwargs):
        super(FeinCMSContentInlineTypeForm, self).__init__(auto_id='id_%s_%%s' % settings.FEINCMS_CONTENTINLINE_NEEDLE, *args, **kwargs)


class FeinCMSContentInlineTypeAdmin(admin.ModelAdmin):
    """
    If the proxy-blocks are handled, return JSON when it was succesfull.
    """

    form = FeinCMSContentInlineTypeForm

    def response_add(self, request, obj, post_url_continue='../%s/'):

        response = super(FeinCMSContentInlineTypeAdmin, self).response_add(request, obj)

        if '_json' in request.REQUEST and request.method == 'POST':

            data = {
                'pk': obj._get_pk_val(),
                'url': reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=(obj._get_pk_val(),)) + '?_popup=1&_json=1',
                'messages': []
            }

            storage = get_messages(request)

            for message in storage:
                data['messages'].append(message.message)

            return HttpResponse(json.dumps(data), content_type='application/json');

        return response

    def response_change(self, request, obj):

        response = super(FeinCMSContentInlineTypeAdmin, self).response_change(request, obj)

        if '_json' in request.REQUEST and request.method == 'POST':

            data = {
                'pk': obj._get_pk_val(),
                'url': reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=(obj._get_pk_val(),)) + '?_popup=1&_json=1',
                'messages': []
            }

            storage = get_messages(request)

            for message in storage:
                data['messages'].append(message.message)

            return HttpResponse(json.dumps(data), content_type='application/json');

        return response

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


class FeinCMSContentInlineTypeInlineFormset(BaseInlineFormSet):

    @classmethod
    def get_default_prefix(cls):
        #import pdb; pdb.set_trace()
        #from django.db.models.fields.related import RelatedObject
        return '%s-%s' % (settings.FEINCMS_CONTENTINLINE_NEEDLE, cls.fk.rel.get_accessor_name().replace('+',''))


def register_dynamics(list):

    class Meta:
        app_label = FeinCMSContentInlineLink._meta.app_label
        proxy = True

    for model_base_name, model_content_instance in list:
        # We are going to use Proxy-models to provide the admin-logic (but we still want to store Blocks)
        model_block = type(model_base_name, (FeinCMSContentInlineLink,), {
            '__module__': __name__,
            'Meta': Meta,
        })

        dynamics = []

        for dynamic, inline_type in model_content_instance.dynamics:
            inline = type(dynamic.__name__ + 'Inline', (admin.StackedInline if inline_type == 'stacked' else admin.TabularInline, ), {
                '__module__': __name__,
                'model': dynamic,
                'extra': 1,
                'formset': FeinCMSContentInlineTypeInlineFormset
            })
            dynamics.append(inline)

        model_admin = type(model_base_name + 'Admin', (FeinCMSContentInlineTypeAdmin,), {
            '__module__': __name__,
            'inlines': dynamics
        })

        admin.site.register(model_block, model_admin)
