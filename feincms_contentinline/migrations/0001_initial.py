# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FeinCMSContentInlineLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'inline link',
                'verbose_name_plural': 'inline links',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ColumnTextBlockContent',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('feincms_contentinline.feincmscontentinlinelink',),
        ),
    ]
