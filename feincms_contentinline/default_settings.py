# -*- coding: utf-8 -*-
"""
Default settings for FeinCMSContentInline

All of these can be overridden by specifying them in the standard
``settings.py`` file.
"""

from django.conf import settings

FEINCMS_CONTENTINLINE_NEEDLE = getattr(settings,
                                       'FEINCMS_CONTENTINLINE_NEEDLE',
                                       '__feincmscontentinline__')
