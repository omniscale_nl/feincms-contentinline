from django.apps import AppConfig

class FeinCMSContentInlineConfig(AppConfig):
    name = 'feincms_contentinline'
    verbose_name = "FeinCMS (Content with inline)"
