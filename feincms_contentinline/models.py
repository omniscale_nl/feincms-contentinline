# -*- coding: utf-8 -*-

from django.db import models
from feincms.admin.item_editor import FeinCMSInline
from feincms.admin.item_editor import ItemEditorForm
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_delete


class FeinCMSContentInlineLink(models.Model):
    """
    Base class to which all inline items will be linked.
    In base/admin.py, proxy-classes will be generated for each contentblock.
    """

    class Meta:
        verbose_name = _("inline link")
        verbose_name_plural = _("inline links")


class FeinCMSContentInlineLine(models.Model):
    """
    Abstract class that will act as the parent for all inline items.
    """

    link = models.ForeignKey(FeinCMSContentInlineLink,
                             null=False,
                             default=None)

    class Meta:
        abstract = True
        verbose_name = _("inline item")
        verbose_name_plural = _("inline items")


class FeinCMSContentInline(FeinCMSInline):
    """
    FeinCMS subclass for the contenttypes on Page (which are inlines
    themselves).  We need to hide the lookup-field since we are going to fill
    it with Javascript.
    """

    raw_id_fields = ('inline_link',)

    def __init__(self, *args, **kwargs):
        super(FeinCMSInline, self).__init__(*args, **kwargs)


def delete_inline(sender, instance, using, **kwargs):

    for dynamic, inline_type in instance.dynamics:
        dynamic.objects.filter(link_id=instance.inline_link_id).delete()

    FeinCMSContentInlineLink.objects.get(pk=instance.inline_link_id).delete()


class FeinCMSContentInlineType(models.Model):
    """
    The base-class for all contenttypes

    This class makes sure that all required includes are loaded and contains
    the default render logic for the contenttypes. Additionally, some settings
    that are specifically for FeinCMS are set so that the admin section will do
    want we want it to do.
    """

    inline_link = models.ForeignKey(FeinCMSContentInlineLink,
                                    null=False,
                                    default=None)

    @classmethod
    def initialize_type(cls):

        cls.add_to_class('feincmscontentinline_hook',
                         models.CharField(max_length=200,
                                          null=True,
                                          blank=True,
                                          default=cls.__name__.lower()))

    def __init__(self, *args, **kwargs):
        super(FeinCMSContentInlineType, self).__init__(*args, **kwargs)
        post_delete.connect(delete_inline, sender=self.__class__)

    feincms_item_editor_inline = FeinCMSContentInline
    feincms_item_editor_includes = {
        'head': [
            'admin/feincms_contentinline/init.html'
        ],
    }

    class Meta:
        abstract = True
