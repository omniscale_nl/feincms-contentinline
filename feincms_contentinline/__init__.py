# -*- coding: utf-8 -*-

VERSION = (0, 0, 7)
__version__ = '.'.join(map(str, VERSION))

default_app_config = 'feincms_contentinline.apps.FeinCMSContentInlineConfig'

class LazySettings(object):
    """
    Shameless copy from feincms/__init__.py
    """

    def _load_settings(self):
        from feincms_contentinline import default_settings
        from django.conf import settings as django_settings

        for key in dir(default_settings):
            if not key.startswith('FEINCMS_CONTENTINLINE_'):
                continue

            value = getattr(default_settings, key)
            value = getattr(django_settings, key, value)
            setattr(self, key, value)

    def __getattr__(self, attr):
        self._load_settings()
        del self.__class__.__getattr__
        return self.__dict__[attr]

settings = LazySettings()
