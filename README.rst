=============================
FeinCMS (Content with inline)
=============================

A small addon to FeinCMS that allows for the management of lines within contenttypes that are registered for a page.


Installation
============

* install using ``pip``: ``pip install git+https://bitbucket.org/omniscale_nl/feincms-contentinline.git#egg=feincms_contentinline``
* add ``feincms_contentinline`` to your INSTALLED_APPS after ``feincms``


Building
========

* for new versions update the version number in ``__init__.py``
* build the new dist using: ``python setup.py sdist``


Usage
=====

``models.py``::

    from feincms_contentinline.models import FeinCMSContentInlineLine
    from feincms_contentinline.models import FeinCMSContentInlineType

    class Link(FeinCMSContentInlineLine):

        title = models.CharField(_("Title"),
                                 max_length=255,
                                 null=False,
                                 blank=False)

        url = models.CharField(_("URL"),
                               max_length=255,
                               null=False,
                               blank=False)

    class Meta:
        verbose_name = _("link")
        verbose_name_plural = _("links")


    class Content(FeinCMSContentInlineType):

        title = models.CharField(_("Titel"),
                                max_length=255,
                                null=True,
                                blank=False)

        text = models.TextField(_("Text"),
                                null=True,
                                blank=True)

        dynamics = [ (Link, 'stacked'), ]

        class Meta:
            abstract = True
            verbose_name = _("Kolomstekst")
            verbose_name_plural = _("Kolomsteksten")

    Page.create_content_type(Content, regions=('main'))

``admin.py``::

    from feincms_contentinline.admin import register_dynamics

    # find all classes which have a 'dynamics' property (which denotes a contenttype with dynamic attributes)
    list = [(name, cls) for name, cls in content_types.base_content.__dict__.items() if isinstance(cls, type) and hasattr(cls, 'dynamics')]

    register_dynamics(list)
